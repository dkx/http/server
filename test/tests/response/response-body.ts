import {expect} from 'chai';
import {ResponseBody} from '../../../lib';


describe('#Response/ResponseBody', () => {

	it('should write chunk', () =>  {
		const data = [];
		const body = new ResponseBody((chunk, encoding, done) => {
			data.push(chunk.toString());
			done();
		});

		body.write('hello');
		body.write('world');

		expect(data).to.be.eql(['hello', 'world']);
	});

});
