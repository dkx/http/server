import {IncomingHttpHeaders} from 'http';
import {Readable} from 'stream';
import {Middleware, NextMiddlewareFunction, RequestState} from '../middleware';
import {Request} from '../request';
import {Response, ResponseBody} from '../response';


export declare interface MiddlewareTestOptions {
	method?: string,
	url?: string,
	headers?: IncomingHttpHeaders,
	body?: string,
	state?: RequestState,
	onBodyWrite?: (chunk: any) => void,
	next?: NextMiddlewareFunction,
}


export async function testMiddleware(middleware: Middleware, options: MiddlewareTestOptions = {}): Promise<Response>
{
	let body: Readable|undefined;

	if (typeof options.body !== 'undefined') {
		body = new Readable;
		body._read = () => {
			body.push(new Buffer(options.body));
			body.push(null);
		};
	}

	const next: NextMiddlewareFunction = typeof options.next === 'undefined' ? async (res) => res : options.next;
	const state: RequestState = typeof options.state === 'undefined' ? {} : options.state;

	const req = new Request(options.method, options.url, options.headers, body);
	const res = await middleware(req, new Response, next, state);

	if (typeof options.onBodyWrite !== 'undefined') {
		const body = new ResponseBody((chunk, encoding, done) => {
			options.onBodyWrite(chunk);
			done();
		});

		res._attachBody(body);
	}

	return res;
}
