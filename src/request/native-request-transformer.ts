import {IncomingMessage} from 'http';
import {Request} from './request';


export function transformFromNativeRequest(request: IncomingMessage): Request
{
	return new Request(
		request.method,
		request.url,
		request.headers,
		request.readable ? request : undefined,
	);
}
