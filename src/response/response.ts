import {append as varyAppend} from 'vary';
import {ResponseBody} from './response-body';


export declare type ResponseHeader = number|string|Array<string>;
export declare type ResponseHeaders = {[name: string]: ResponseHeader};

export declare type ResponseBodyAttachedListener = (body: ResponseBody) => void;


export declare interface ResponseModifyData
{
	statusCode?: number,
	statusMessage?: string,
	headers?: ResponseHeaders,
}


export class Response
{


	constructor(
		public readonly statusCode: number = 200,
		public readonly statusMessage: string = '',
		private readonly _headers: ResponseHeaders = {},
		private readonly bodyEvents: Array<ResponseBodyAttachedListener> = [],
	) {}


	get headers(): ResponseHeaders
	{
		return {...this._headers};
	}


	public write(chunk: any): void
	{
		if (!chunk.length) {
			return;
		}

		this.bodyEvents.push((body) => body.write(chunk));
	}


	public withStatus(code: number, message: string = ''): Response
	{
		return this.clone({
			statusCode: code,
			statusMessage: message,
		});
	}


	public hasHeader(name: string): boolean
	{
		name = name.toLowerCase();
		return typeof this._headers[name] !== 'undefined';
	}


	public getHeader(name: string, defaultValue?: ResponseHeader): ResponseHeader|undefined
	{
		name = name.toLowerCase();
		return this.hasHeader(name) ?
			this._headers[name] :
			defaultValue
		;
	}


	public withHeader(name: string, value: ResponseHeader): Response
	{
		const append: ResponseHeaders = {};
		name = name.toLowerCase();
		append[name] = value;

		return this.clone({
			headers: {...this._headers, ...append},
		});
	}


	public withVaryHeader(field: string|Array<string>): Response
	{
		const prev: ResponseHeader = this.getHeader('Vary', '');
		const header: string = Array.isArray(prev) ? prev.join(', ') : String(prev);

		const next = varyAppend(header, field);

		return next ?
			this.withHeader('Vary', next) :
			this.clone()
		;
	}


	public removeHeader(name: string): Response
	{
		name = name.toLowerCase();

		if (!this.hasHeader(name)) {
			return this.clone();
		}

		const headers = {...this._headers};
		delete headers[name];

		return this.clone({
			headers: headers,
		});
	}


	public clone(data: ResponseModifyData = {}): Response
	{
		return new Response(
			typeof data.statusMessage === 'undefined' ? this.statusCode : data.statusCode,
			typeof data.statusMessage === 'undefined' ? this.statusMessage : data.statusMessage,
			typeof data.headers === 'undefined' ? {...this._headers} : data.headers,
			this.bodyEvents,
		);
	}


	public _attachBody(body: ResponseBody): void
	{
		for (let event of this.bodyEvents) {
			event(body);
		}
	}


	public _getBodyEventsCount(): number
	{
		return this.bodyEvents.length;
	}

}
