import {ServerResponse} from 'http';
import {Response} from './response';
import {ResponseBody} from './response-body';


export function transformToNativeResponse(nativeResponse: ServerResponse, response: Response, end: () => void): void
{
	nativeResponse.statusCode = response.statusCode;
	nativeResponse.statusMessage = response.statusMessage;

	for (let headerName in response.headers) {
		if (response.headers.hasOwnProperty(headerName)) {
			nativeResponse.setHeader(headerName, response.headers[headerName]);
		}
	}

	const bodyEventsCount: number = response._getBodyEventsCount();

	if (bodyEventsCount === 0) {
		end();
		return;
	}

	let written = 0;
	const body = new ResponseBody((chunk, encoding, done) => {
		nativeResponse.write(chunk, encoding, () => {
			done();
			written++;

			if (written === bodyEventsCount) {
				end();
			}
		});
	});

	response._attachBody(body);
}
