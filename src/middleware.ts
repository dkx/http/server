import {Request} from './request';
import {Response} from './response';


export declare interface RequestState
{
	[key: string]: any,
}

export type NextMiddlewareFunction = (response: Response) => Promise<Response>;
export type Middleware = (request: Request, response: Response, next: NextMiddlewareFunction, state: RequestState) => Promise<Response>;
