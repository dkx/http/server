import {expect} from 'chai';
import {Readable} from 'stream';
import {Request} from '../../../lib';


describe('#Request/Request', () => {

	describe('constructor()', () => {

		it('should store values', () => {
			const req = new Request('POST', '/users', {'X-Test': 'hello'}, new Readable);

			expect(req.method).to.be.equal('POST');
			expect(req.url).to.be.equal('/users');
			expect(req.headers).to.be.eql({'X-Test': 'hello'});
			expect(req.body).to.be.an.instanceOf(Readable);
		});

	});

	describe('hasHeader()', () => {

		it('should test if header exists', () => {
			const req = new Request('GET', '/', {'x-test': 'hello'});

			expect(req.hasHeader('X-Test')).to.be.equal(true);
			expect(req.hasHeader('X-Test-unknown')).to.be.equal(false);
		});

	});

	describe('getHeader()', () => {

		it('should return header', () => {
			const req = new Request('GET', '/', {'x-test': 'hello'});

			expect(req.getHeader('X-Test')).to.be.equal('hello');
		});

	});

});
