import {expect} from 'chai';
import {testMiddleware, Middleware} from '../../../lib';


describe('#Testing/Middleware/testMiddleware', () => {

	it('should set all request options', async () => {
		let called: boolean = false;

		const middleware: Middleware = (req, res, next, state) => {
			called = true;

			expect(req.method).to.be.equal('POST');
			expect(req.url).to.be.equal('/v1/users/5');
			expect(req.headers).to.be.eql({
				'X-Test': 'hello world',
			});
			expect(state).to.be.eql({
				ver: 1,
			});

			const body = [];
			return new Promise((resolve) => {
				req.body.on('data', (chunk) => body.push(chunk.toString()));
				req.body.on('end', () => {
					expect(body).to.be.eql(['lorem ipsum']);
					resolve(next(res));
				});
			});
		};

		await testMiddleware(middleware, {
			method: 'POST',
			url: '/v1/users/5',
			headers: {
				'X-Test': 'hello world',
			},
			state: {
				ver: 1,
			},
			body: 'lorem ipsum',
		});

		expect(called).to.be.equal(true);
	});

	it('should listen to response body writes', async () => {
		const data: Array<any> = [];
		const middleware: Middleware = (req, res, next) => {
			res.write('hello');
			res.write(' ');
			res.write('world');

			return next(res);
		};

		await testMiddleware(middleware, {
			onBodyWrite: (chunk) => data.push(chunk.toString()),
		});

		expect(data).to.be.eql(['hello', ' ', 'world']);
	});

	it('should add custom next function', async () => {
		let called: boolean = false;
		const middleware: Middleware = (req, res, next) => {
			return next(res);
		};

		await testMiddleware(middleware, {
			next: async (res) => {
				called = true;
				return res;
			},
		});

		expect(called).to.be.equal(true);
	});

});
