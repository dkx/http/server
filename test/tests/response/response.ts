import {expect} from 'chai';
import {Response, ResponseBody} from '../../../lib';


let res: Response;


describe('#Response/Response', () => {

	beforeEach(() => {
		res = new Response;
	});

	describe('constructor()', () => {

		it('should store default values', () => {
			res = new Response(404, 'Not found', {
				'X-Test': 'hello world',
			});

			expect(res.statusCode).to.be.equal(404);
			expect(res.statusMessage).to.be.equal('Not found');
			expect(res.headers).to.be.eql({
				'X-Test': 'hello world',
			});
		});

	});

	describe('clone()', () => {

		it('should clone response', () => {
			expect(res.clone()).to.not.be.equal(res);
		});

	});

	describe('withStatus()', () => {

		it('should change status', () => {
			res = new Response(404, 'Not found');

			const updated = res.withStatus(200, 'OK');

			expect(res.statusCode).to.be.equal(404);
			expect(res.statusMessage).to.be.equal('Not found');
			expect(updated).to.not.be.equal(res);
			expect(updated.statusCode).to.be.equal(200);
			expect(updated.statusMessage).to.be.equal('OK');
		});

	});

	describe('write()', () => {

		it('should write data into body', () => {
			res = new Response;
			res.write('hello');
			res.write(' ');
			res.write('world');

			const data = [];
			const body = new ResponseBody((chunk, encoding, done) => {
				data.push(chunk.toString());
				done();
			});

			res._attachBody(body);

			expect(data).to.be.eql([
				'hello',
				' ',
				'world',
			]);
		});

	});

	describe('hasHeader()', () => {

		it('should return true when header exists', () => {
			const updated = res.withHeader('X-TEST', 'hello world');

			expect(updated.hasHeader('x-test')).to.be.equal(true);
			expect(updated.hasHeader('X-TEST')).to.be.equal(true);
		});

		it('should return false when header does not exists', () => {
			expect(res.hasHeader('X-Test')).to.be.equal(false);
		});

	});

	describe('getHeader()', () => {

		it('should return undefined when header does not exists', () => {
			expect(res.getHeader('X-Test')).to.be.equal(undefined);
		});

		it('should return header', () => {
			const updated = res.withHeader('X-TEST', 'hello world');

			expect(updated.getHeader('X-TEST')).to.be.equal('hello world');
			expect(updated.getHeader('x-test')).to.be.equal('hello world');
		});

		it('should return default value when header does not exists', () => {
			expect(res.getHeader('X-Test', 'hello world')).to.be.equal('hello world');
		});

	});

	describe('withHeader()', () => {

		it('should change header', () => {
			const updated = res.withHeader('X-TEST', 'hello');

			expect(res.headers).to.be.eql({});
			expect(updated).to.not.be.equal(res);
			expect(updated.headers).to.be.eql({
				'x-test': 'hello',
			});
		});

	});

	describe('withVaryHeader()', () => {

		it('should add new vary header', () => {
			const updated = res.withVaryHeader('Accept');

			expect(updated.headers).to.be.eql({
				vary: 'Accept',
			});
		});

		it('should append vary header to existing string vary header', () => {
			const updated = res
				.withHeader('Vary', 'Accept-Encoding')
				.withVaryHeader('Accept');

			expect(updated.headers).to.be.eql({
				vary: 'Accept-Encoding, Accept',
			});
		});

		it('should append vary header to existing array vary header', () => {
			const updated = res
				.withHeader('Vary', ['Accept-Encoding'])
				.withVaryHeader('Accept');

			expect(updated.headers).to.be.eql({
				vary: 'Accept-Encoding, Accept',
			});
		});

	});

	describe('removeHeader()', () => {

		it('should just clone response when header does not exists', () => {
			const updated = res.removeHeader('X-Test');

			expect(res.headers).to.be.eql({});
			expect(updated).to.not.be.equal(res);
			expect(updated.headers).to.be.eql({});
		});

		it('should remove header', () => {
			res = new Response(200, 'OK', {
				'x-test': 'hello',
			});

			const updated = res.removeHeader('X-Test');

			expect(res.headers).to.be.eql({
				'x-test': 'hello',
			});
			expect(updated).to.not.be.equal(res);
			expect(updated.headers).to.be.eql({});
		});

	});

	describe('_getBodyEventsCount()', () => {

		it('should return count of attached events', () => {
			expect(res._getBodyEventsCount()).to.be.equal(0);

			res.write('a');
			res.write('b');
			res.write('c');

			res.write('');

			expect(res._getBodyEventsCount()).to.be.equal(3);
		});

	});

	describe('_attachBody()', () => {

		it('should attach response body and call all events', () => {
			res.write('hello world');

			const data = [];
			const body = new ResponseBody((chunk, encoding, done) => {
				data.push(chunk.toString());
				done();
			});

			res._attachBody(body);

			expect(data).to.be.eql(['hello world']);
		});

	});


});
