import {Writable, WritableOptions} from 'stream';


export class ResponseBody extends Writable
{


	constructor(
		private onWrite: (chunk: any, encoding: string, done: (error?: Error|null) => void) => void,
		options: WritableOptions = {},
	) {
		super(options);
	}


	public _write(chunk: any, encoding: string, done: (error?: Error|null) => void): void
	{
		this.onWrite(chunk, encoding, done);
	}

}
