import {Request, Response, ResponseHeaders, NextMiddlewareFunction, Middleware} from '../../lib';


export declare interface ResponseMiddlewareOptions
{
	statusCode?: number,
	statusMessage?: string,
	body?: string,
	headers?: ResponseHeaders,
}


export function responseMiddleware(options: ResponseMiddlewareOptions): Middleware
{
	return (req: Request, res: Response, next: NextMiddlewareFunction): Promise<Response> =>
	{
		if (typeof options.statusCode !== 'undefined') {
			res = res.withStatus(options.statusCode, options.statusMessage);
		}

		if (typeof options.headers !== 'undefined') {
			for (let headerName in options.headers) {
				if (options.headers.hasOwnProperty(headerName)) {
					res = res.withHeader(headerName, options.headers[headerName]);
				}
			}
		}

		if (typeof options.body !== 'undefined') {
			res.write(options.body);
		}

		return next(res);
	};
}
