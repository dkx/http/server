# DKX/Http/Server

Super simple HTTP server with middlewares support for node.js.

## Installation

```bash
$ npm install --save @dkx/http-server
```

or with yarn

```bash
$ yarn add @dkx/http-server
```

## Basic usage

```js
const {Server} = require('@dkx/http-server');
const {someRandomMiddleware} = require('some-random-middleware');

const app = new Server;

app.use(someRandomMiddleware);

app.run(8080, () => {
    console.log('Server is running on port 8080');
});
```

## Writing middlewares

Each middleware is just an ordinary async function.

Middleware must call the `next` function with current `response`.

```js
const {Server} = require('@dkx/http-server');

async function appendHeaderMiddleware(request, response, next)
{
    response = response.withHeader('X-My-custom-header', 'hello world');
    return next(response);
}

const app = new Server;

app.use(appendHeaderMiddleware);
```

## Writing response data

```js
function writeResponseDataMiddleware(request, response, next)
{
    response.write('hello');
    response.write(' ');
    response.write('world');
    
    return next(response);
}
```

## Passing data between middlewares

```js
function middlewareA(request, response, next, state)
{
    state.message = 'hello world';
    return next(response);
}

function middlewareB(request, response, next, state)
{
    console.log(state.message);    // output: hello world
    return next(response);
}
```

## Testing middlewares

Testing new middlewares is really easy with the built in helper function. It construct all the necessary objects and 
runs the middleware for you.

```js
const {testMiddleware} = require('@dkx/http-server');

function myUselessMiddleware(request, response, next)
{
	response.write('hello world');
    response = response.withHeader('X-Middleware-header', 'lorem ipsum');
    return next(response);
}

const data = [];
const response = await testMiddleware(myUselessMiddleware, {
    method: 'POST',
    url: '/v1/users/5',
    body: 'some HTTP body',
    headers: {
        'X-My-custom-http-header': 'hello world'
    },
    state: {
        msg: 'My custom shared state'
    },
    onBodyWrite: function(chunk) {
    	data.push(chunk.toString());
    },
    next: async function(res) {
    	console.log('Custom next function called');
    	return res;
    },
});

console.log(response.getHeader('X-Middleware-header'));  // output: "lorem ipsum"
console.log(data);  // output: ["hello world"]
``` 

The `testMiddleware` function can be used just with your middleware:

```js
testMiddleware(myUselessMiddleware);
```

## Server object

* **`use`**

    Append middleware.

    **Arguments:**

    `middleware: Middleware`: middleware to attach.

* **`run()`**

    Start the HTTP server.

    **Arguments:**

    `port: number`: port where the HTTP server will be listening for new requests.
    `fn: () => void`: callback called when server is ready to handle requests.

* **`close()`**

    Stop running HTTP server.

    **Arguments:**

    `fn: () => void`: callback called when server is completely shut down.
    
* **`middleware()`**

	Run custom `Request` and `Response`.
	
	**Arguments:**
	
	`request: Request`: Custom `Request` object
	`response: Response`: Custom `Response` object
	
	**Return:**
	
	`Promise<Response>`: New response after running through the middleware stack.

## Request object

* **`method`**

    Contains request method (`GET`, `POST`, ...).

* **`url`**

    Contains requested URL.

* **`headers`**

    Contains request HTTP headers.

* **`body`**

    [Readable stream](https://nodejs.org/api/stream.html#stream_readable_streams) for accessing request data.

* **`hasHeader()`**

	Test if header exists.

	**Arguments:**
	
	`name: string`: Name of header.
	
	**Return:**
	
	`boolean`

* **`getHeader()`**

	Return HTTP header.

	**Arguments:**
	
	`name: string`: Name of header.
	
	**Return:**
	
	`string|Array<string>|undefined`

## Response object

The `Response` is an immutable object.

* **`statusCode`**

    Response status code, default `200`.

* **`statusMessage`**

    Response status message, default is an empty string.

* **`headers`**

    Contains list of currently returned HTTP headers.

* **`write()`**

    Method for writing data into response body.
    
    **Arguments:**
    
    `chunk: any`: data to write.

* **`withStatus()`**

    Write response status.

    **Arguments:**

    `code: number`: new response status code.
    `message: string`: new response status message, default is an empty string.

    **Return:**

    `Response`: cloned `Response` object with modified status.
    
* **`hasHeader()`**

    Check whether header exists.
    
    **Arguments:**
    
    `name: string`: name of HTTP header to check.
    
    **Return:**
    
    `boolean`
    
* **`getHeader()`**

    Get HTTP response header.
    
    **Arguments:**
    
    `name: string`: name of HTTP header.
    `defaultValue: undefined|string|number|Array<string>`: default value to return if HTTP header does not exists.
    
    **Return:**
    
    `undefined|string|number|Array<string>`

* **`withHeader()`**

    Write response header.

    **Arguments:**

    `name: string`: name of the new header.
    `value: string`: value of the new header.

    **Return:**

    `Response`: cloned `Response` object with modified headers.

* **`withVaryHeader()`**

    Write vary HTTP response header.
    
    **Arguments:**
    
    `field: string|Array<string>`: name of header you wish to add into vary header
    
    **Return:**
    
    `Response`: cloned `Response` object with modified headers.

* **`removeHeader()`**

    Remove response header.

    **Arguments:**

    `name: string`: name of the removed header.

    **Return:**

    `Response`: cloned `Response` object with modified headers.

## ResponseBody object

Writable stream for writing the response data.

* **`write()`**

    Write response chunk

    **Arguments:**

    `chunk`: chunk of data to write.
