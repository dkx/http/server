import {expect} from 'chai';
import fetch from 'node-fetch';
import {Server, Request, Response, Middleware, NextMiddlewareFunction} from '../../lib';
import {responseMiddleware} from '../helpers';


let server: Server;


describe('#Server', () => {

	beforeEach(() => {
		server = new Server;
	});

	describe('run()', () => {

		beforeEach((done) => {
			server.run(8000, done);
		});

		afterEach((done) => {
			server.close(done);
		});

		it('should should throw an error if server is already running', () => {
			expect(() => {
				server.run(9000);
			}).to.throw(Error, 'Server is already running');
		});

		it('should do a simple HTTP request', async () => {
			server.use(responseMiddleware({
				statusCode: 404,
				statusMessage: 'Not found',
				body: 'Error: data not found',
				headers: {
					'X-Test': 'hello world',
				},
			}));

			const res = await fetch('http://localhost:8000');

			expect(res.status).to.be.equal(404);
			expect(res.statusText).to.be.equal('Not found');
			expect(res.headers.has('X-Test')).to.be.equal(true);
			expect(res.headers.get('X-Test')).to.be.equal('hello world');
			expect(await res.text()).to.be.equal('Error: data not found');
		});

		it('should attach more middlewares', async () => {
			server.use(responseMiddleware({
				body: 'Hello',
			}));

			server.use(responseMiddleware({
				body: ' ',
			}));

			server.use(responseMiddleware({
				body: 'world',
			}));

			const res = await fetch('http://localhost:8000');

			expect(await res.text()).to.be.equal('Hello world');
		});

		it('should send without body', async () => {
			server.use(responseMiddleware({}));

			const res = await fetch('http://localhost:8000');

			expect(await res.text()).to.be.equal('');
		});

		it('should send empty body', async () => {
			server.use(responseMiddleware({
				body: '',
			}));

			const res = await fetch('http://localhost:8000');

			expect(await res.text()).to.be.equal('');
		});

	});

	describe('middleware()', () => {

		it('should run middlewares', async () => {
			const req = new Request;
			const res = new Response;

			let called: boolean = false;

			const a: Middleware = async (request: Request, response: Response, next: NextMiddlewareFunction) => {
				called = true;
				return next(response);
			};

			server.use(a);

			await server.middleware(req, res);

			expect(called).to.be.equal(true);
		});

	});

	describe('close()', () => {

		it('should call callback when server is not running', (done) => {
			server.close(done);
		});

	});

	describe('isRunning()', () => {

		it('should indicate if server is running', (done) => {
			expect(server.isRunning()).to.be.equal(false);

			server.run(8080, () => {
				expect(server.isRunning()).to.be.equal(true);

				server.close(() => {
					expect(server.isRunning()).to.be.equal(false);
					done();
				});
			});
		});

	});

});
