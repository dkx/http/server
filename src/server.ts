import {Middleware, RequestState} from './middleware';
import {Request, transformFromNativeRequest} from './request';
import {Response, transformToNativeResponse} from './response';
import {MiddlewareStack} from './middleware-stack';
import {Server as HttpServer, createServer} from 'http';


export class Server
{


	private server: HttpServer|undefined;


	constructor(
		private middlewares: Array<Middleware> = [],
	) {}


	public use(middleware: Middleware): void
	{
		this.middlewares.push(middleware);
	}


	public run(port: number, fn?: () => void): void
	{
		if (this.server) {
			throw new Error('Server is already running');
		}

		this.server = createServer(async (request, response) => {
			const req = transformFromNativeRequest(request);
			const res = await this.middleware(req, new Response, {});

			transformToNativeResponse(response, res, () => {
				response.end();
			});
		});

		this.server.listen(port, fn);
	}


	public middleware(req: Request, res: Response, state: RequestState = {}): Promise<Response>
	{
		const middlewares = new MiddlewareStack([...this.middlewares]);
		return middlewares.run(req, res, state);
	}


	public close(fn?: () => void): void
	{
		if (this.server) {
			this.server.close(fn);
			this.server = undefined;
		} else {
			fn();
		}
	}


	public isRunning(): boolean
	{
		return typeof this.server !== 'undefined';
	}

}
