import {IncomingHttpHeaders} from 'http';
import {Readable} from 'stream';


export class Request
{


	constructor(
		public readonly method: string = 'GET',
		public readonly url: string = '',
		public readonly headers: IncomingHttpHeaders = {},
		public readonly body?: Readable,
	) {}


	public hasHeader(name: string): boolean
	{
		name = name.toLowerCase();
		return typeof this.headers[name] !== 'undefined';
	}


	public getHeader(name: string): string|Array<string>|undefined
	{
		name = name.toLowerCase();
		return this.headers[name];
	}

}
