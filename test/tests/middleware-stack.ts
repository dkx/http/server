import {expect} from 'chai';
import {MiddlewareStack, Middleware, NextMiddlewareFunction, Request, Response} from '../../lib';


let request: Request;
let response: Response;


describe('#MiddlewareStack', () => {

	beforeEach(() => {
		request = new Request;
		response = new Response;
	});

	describe('run()', () => {

		it('should run middlewares', async () => {
			const log: Array<string> = [];

			const a: Middleware = async (request: Request, response: Response, next: NextMiddlewareFunction) => {
				log.push('a');
				return next(response);
			};

			const b: Middleware = async (request: Request, response: Response, next: NextMiddlewareFunction) => {
				log.push('b');
				return next(response);
			};

			const c: Middleware = async (request: Request, response: Response, next: NextMiddlewareFunction) => {
				log.push('c');
				return next(response);
			};

			const stack = new MiddlewareStack([a, b, c]);
			await stack.run(request, response);

			expect(log).to.be.eql(['a', 'b', 'c']);
		});

		it('should run middlewares in reverse order', async () => {
			const log: Array<string> = [];

			const a: Middleware = async (request: Request, response: Response, next: NextMiddlewareFunction) => {
				const result = await next(response);
				log.push('a');
				return result;
			};

			const b: Middleware = async (request: Request, response: Response, next: NextMiddlewareFunction) => {
				const result = await next(response);
				log.push('b');
				return result;
			};

			const c: Middleware = async (request: Request, response: Response, next: NextMiddlewareFunction) => {
				const result = await next(response);
				log.push('c');
				return result;
			};

			const stack = new MiddlewareStack([a, b, c]);
			await stack.run(request, response);

			expect(log).to.be.eql(['c', 'b', 'a']);
		});

		it('should stop middlewares sooner', async () => {
			const log: Array<string> = [];

			const a: Middleware = async (request: Request, response: Response, next: NextMiddlewareFunction) => {
				log.push('a-before');
				const result = await next(response);
				log.push('a-after');
				return result;
			};

			const b: Middleware = async (request: Request, response: Response) => {
				log.push('b');
				return response;
			};

			const c: Middleware = async (request: Request, response: Response, next: NextMiddlewareFunction) => {
				log.push('c');
				return next(response);
			};

			const stack = new MiddlewareStack([a, b, c]);
			await stack.run(request, response);

			expect(log).to.be.eql(['a-before', 'b', 'a-after']);
		});

	});

});
