import {Request} from './request';
import {Response} from './response';
import {Middleware, NextMiddlewareFunction, RequestState} from './middleware';


export class MiddlewareStack
{


	constructor(
		private readonly middlewares: Array<Middleware>,
	) {}


	public async run(request: Request, response: Response, state: RequestState = {}): Promise<Response>
	{
		if (!this.middlewares.length) {
			return response;
		}

		const next = this.createNextFunction(request, state, 0);

		return await this.middlewares[0](request, response, next, state);
	}


	private createNextFunction(request: Request, state: RequestState, current: number): NextMiddlewareFunction
	{
		const next = this.middlewares[current + 1];

		if (!next) {
			return async (response: Response) => response;
		}

		return (response: Response) => {
			return next(request, response, this.createNextFunction(request, state, ++current), state);
		};
	}

}
